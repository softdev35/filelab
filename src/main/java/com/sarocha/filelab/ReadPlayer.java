/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sarocha.filelab;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Sarocha
 */
public class ReadPlayer {

    public static void main(String[] args) {

        Player O = null;
        Player X = null;
        FileInputStream fis = null;
        try {
            File file = new File("players.dat");
            fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            O = (Player) ois.readObject();
            X = (Player) ois.readObject();
            ois.close();
            fis.close();
            System.out.println(O);
            System.out.println(X);
        } catch (FileNotFoundException ex) {
            O = new Player('O');
            X = new Player('X');
        } catch (IOException ex) {

        } catch (ClassNotFoundException ex) {

        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(ReadPlayer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
